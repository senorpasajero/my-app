const express = require('express');
const router = express.Router();
const System = require('../models/system');

const err = (err, res) => {
  res.status(501).json({
    status: 501,
    message: typeof err === 'object' ? err.message : err
  });
}

router.get('/', (req, res) => {
  System.find({}, function(err, systems) {
    if (err) {
      return err(err);
    }
    res.json({
      status: 200,
      data: systems,
      message: null
    })
  });
});

router.post('/', (req, res) => {
  let system = new System(req.body.system);
  system.save()
    .then((s) => {
      res.json({
        status: 200,
        data: system,
        message: null
      });
    }).catch(err => err(err));
});

router.put('/:id', (req, res) => {
  System.findByIdAndUpdate(req.params.id, req.body.system, (err, system) => {
    if (err) {
      return err(err);
    }

    res.json({
      status: 200,
      data: system,
      message: null
    });
  });
});

module.exports = router;
