var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var systemSchema = new Schema({
  systemId: Schema.Types.ObjectId,
  systemName: { type: String, required: true, unique: true },
  brand: { type: String, required: true},
  customerGroups: [Boolean],
  customizationOffers: [{
      code: String,
      name: String
  }],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

var System = mongoose.model('System', systemSchema);

module.exports = System;