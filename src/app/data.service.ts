import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Observable, BehaviorSubject} from 'rxjs';
import 'rxjs/add/operator/map';
import { System } from './create/system';


@Injectable()
export class DataService {

  results: System[];
  
  private _systems: BehaviorSubject<System[]>;
  public readonly systems: Observable<System[]>;

  constructor(private http: Http) {
    this._systems = new BehaviorSubject([]);
    this.systems = this._systems.asObservable();

    this.getData().subscribe((systems) => {
      this._systems.next(systems);
    }, err => console.log(err));
  }

  getData(): Observable<System[]> {
    return this.http.get('/api')
      .map(result => {
        this.results = result.json().data.map(system => {
          return new System(system)
        });
        return this.results;
      });
  }

  postData(system: System): Observable<System> {
    return this.http.post('/api', { system: system})
      .map(result => {
        console.log(result.json().data);
        let _systems = this._systems.getValue();
        let newSystem = new System(result.json().data);
        _systems.push(newSystem);
        this._systems.next(_systems);

        return newSystem;
      });
  }

  putData(system: System): Observable<System> {
    return this.http.put(`/api/${system.systemId}`, {system: system})
      .map(result => <System>result.json().data)
  }

  getSystem(id: string): System {
    return this._systems.getValue().filter(system => system.systemId === id)[0];
  }
}
