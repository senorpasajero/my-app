import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { System } from './create/system'
import { DataService } from './data.service';

@Injectable()
export class DetailsService implements Resolve<System> {

  constructor( private dataService: DataService) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.dataService.getSystem(route.paramMap.get('id'));
  }

}
