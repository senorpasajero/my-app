import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { System } from '../create/system';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-systems',
  templateUrl: './systems.component.html',
  styleUrls: ['./systems.component.css']
})
export class SystemsComponent implements OnInit {

  systems: Observable<System[]>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.systems = this.dataService.systems;
    // this.dataService.getData().subscribe(systems => this.systems = systems);
  }

}
