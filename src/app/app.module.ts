import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DataService } from './data.service';
import { SystemsComponent } from './systems/systems.component';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';
import { DetailsService } from './details.service';

const appRoutes = [
  { path: '', redirectTo: '/systems', pathMatch: 'full' },
  { path: 'systems', component: SystemsComponent }, 
  { path: 'new', component: CreateComponent },
  { path: 'details/:id', component: DetailsComponent, resolve: { system: DetailsService }  }
];

@NgModule({
  declarations: [
    AppComponent,
    SystemsComponent,
    CreateComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true })
  ],
  providers: [DataService, DetailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
