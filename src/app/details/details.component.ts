import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { System } from '../create/system';
import { groups } from '../create/create.consts';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  systemForm: FormGroup;
  system: System;
  groups = groups;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private fb: FormBuilder, 
    private dataService: DataService) { 
    this.system = <System>this.route.snapshot.data['system'];

    this.systemForm = this.fb.group({
      systemId: [this.system.systemId, Validators.required],
      systemName: [this.system.systemName, Validators.required],
      brand: [this.system.brand, Validators.required],
      customerGroups: this.fb.array(this.groups.map((g, i) => this.fb.control(this.system.customerGroups[i]))),
      customizationOffers: this.fb.array(this.system.customizationOffers.map( customizationOffer => this.fb.group({
        code: [customizationOffer.code, Validators.required],
        name: [customizationOffer.name, Validators.required]
      })))
    });
  }

  ngOnInit() {
    
  }

  addOffer(): void {
    const offers = <FormArray>this.systemForm.get('customizationOffers');
    offers.push(this.fb.group({
      code: ['', Validators.required],
      name: ['', Validators.required]
    }));
  }

  onSubmit(): void {
    this.dataService.putData(this.systemForm.value).subscribe(() => this.router.navigate(['/systems']))
  }

}
