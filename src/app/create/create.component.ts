import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { groups } from './create.consts';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  systemForm: FormGroup;
  groups = groups;

  constructor( 
    private router: Router, 
    private fb: FormBuilder, 
    private dataService: DataService) {
    this.systemForm = this.fb.group({
      systemName: ['', Validators.required],
      brand: ['', Validators.required],
      customerGroups: this.fb.array(this.groups.map(g => this.fb.control(g.selected))),
      customizationOffers: this.fb.array([ this.fb.group({
        code: ['', Validators.required],
        name: ['', Validators.required]
      })])
    });
  }

  ngOnInit() {
  }

  addOffer(): void {
    const offers = <FormArray>this.systemForm.get('customizationOffers');
    offers.push(this.fb.group({
      code: ['', Validators.required],
      name: ['', Validators.required]
    }));
  }

  onSubmit() {
    console.log('Submit');
    // this.dataService.postData(this.systemForm.value).subscribe((res) => {
    //   console.log(res);
    //   this.router.navigate(["/systems"]);
    // });
    this.dataService.postData(this.systemForm.value).subscribe(() => this.router.navigate(["/systems"]));
  }

}
