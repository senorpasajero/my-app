export class System {
  systemId: string;
  systemName: string;
  brand: string;
  customerGroups: string[];
  customizationOffers: Offer[];

  constructor({ systemName, brand, customerGroups, customizationOffers, _id }) {
    this.systemName = systemName;
    this.brand = brand;
    this.customerGroups = customerGroups;
    this.customizationOffers = customizationOffers;
    this.systemId = _id;
  }
}

export class Offer {
  code: string;
  name: string;
}

